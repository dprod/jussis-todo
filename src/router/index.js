import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/components/views/Index'
import Project from '@/components/views/Project'

Vue.use(Router)

const router = new Router({
	// mode: 'history',
	routes: [
		{
			path: '/',
			name: 'index',
			component: Index,
		},
		{
			path: '/project/:slug',
			name: 'project',
			component: Project,
		},
	]
})

router.beforeEach((to, from, next) => {
	
	if(to.params.slug)
		document.title = 'jussis-todo › ' + to.params.slug
	else
		document.title = 'jussis-todo'
	
	next()
})

export default router
