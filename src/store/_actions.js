
import Vue from 'vue'

import api from '../api'

import slug from 'slug'

export default {

	// INIT / DATA LOAD

	async init(store) {

		// Load data from API...
		await store.dispatch('fetchProjects')

		// ...then set selected project slug (=> update getter)...
		store.commit('SET_SELECTED_PROJECT_SLUG', store.state.route.params.slug)

		// ...then init our app and go!
		store.commit('INIT', true)

	},

	async fetchProjects(store) {

		const projects = await api.get('/projects/')
		store.commit('SET_PROJECTS', projects)

	},

	// DATA EDIT FUNCTIONS

	// Project

	async addNewProject(store, newProject = store.state.temp.project) {
	
		// #TODO
		// Create object, set values.
		const project = newProject
		Vue.set(project, 'name', newProject.name)
		Vue.set(project, 'description', newProject.description)
		Vue.set(project, 'slug', slug(newProject.name).toLowerCase())
		Vue.set(project, 'todos', [])

		// Add to app.
		store.commit('ADD_NEW_PROJECT', project)

		store.dispatch('showMessage', `Project: ${project.name} added.`)

		// Save to db.
		const { data } = await api.post('/create/project/', project)

		// Get & set project id from db.
		// #TODO
		Vue.set(project, '_id', data._id)

	},

	validateProjectName(store, projectName) {

		if(!projectName)
			return false

		if(store.state.projects.find((project) => project.name.toLowerCase() === projectName.toLowerCase())) {
			store.dispatch('showMessage', `Project: ${projectName} exists! Try a different project name.`)
			return false
		}
		else {
			return true
		}

	},

	async updateProject(store, { oldProject, newProject }) {

		newProject.slug = slug(newProject.name).toLowerCase()

		store.commit('UPDATE_PROJECT', { oldProject, newProject })

		store.dispatch('showMessage', `Project: ${newProject.name} updated!`)

		await api.post('/update/project/', newProject)

	},

	async removeProject(store) {

		const project = store.state.temp.project
		const projectName = project.name

		store.commit('REMOVE_PROJECT', project)

		store.dispatch('showMessage', `Project: ${projectName} removed.`)

		store.commit('HIDE_MODAL')

		await api.post('/delete/project/', project)

	},

	// Todo

	async addNewTodo(store, newTodo = store.state.temp.todo) {

		const project = store.getters.getSelectedProject

		const todo = newTodo
		Vue.set(todo, 'name', newTodo.name)
		Vue.set(todo, 'description', newTodo.description)
		Vue.set(todo, 'slug', slug(newTodo.name).toLowerCase())
		Vue.set(todo, 'project_id', project._id)
		Vue.set(todo, 'completed', false)

		store.commit('ADD_NEW_TODO', { project, todo })

		store.dispatch('showMessage', `Todo: ${todo.name} added.`)

		const { data } = await api.post('/create/todo/', todo)
		Vue.set(todo, '_id', data._id)

	},

	async updateTodo(store, { oldTodo, newTodo }) {

		newTodo.slug = slug(newTodo.name).toLowerCase()

		store.commit('UPDATE_TODO', { oldTodo, newTodo })

		store.dispatch('showMessage', `Todo: ${newTodo.name} updated!`)

		await api.post('/update/todo/', newTodo)

	},

	async removeTodo(store) {

		// #BUG?: payload: { project = store.getters.getSelectedProject, todo = store.state.temp.todo }
		// store.getters.getSelectedProject returns object._id, not object itself...

		const project = store.getters.getSelectedProject
		const todo = store.state.temp.todo
		const todoName = todo.name

		store.commit('REMOVE_TODO', { project, todo })

		store.dispatch('showMessage', `Todo: ${todoName} removed.`)

		store.commit('HIDE_MODAL')

		await api.post('/delete/todo/', todo)

	},

	async completeTodo(store, todo) {

		store.commit('COMPLETE_TODO', todo)

		store.dispatch('showMessage', `Todo: ${todo.name} completed!`)

		await api.post('/update/todo/', todo)

	},

	async uncompleteTodo(store, todo) {

		store.commit('UNCOMPLETE_TODO', todo)

		store.dispatch('showMessage', `Todo: ${todo.name} uncompleted.`)

		await api.post('/update/todo/', todo)

	},

	// UI/VIEW FUNCTIONS

	// Select project

	showSelectProject(store) {

		store.commit('SHOW_MODAL', {
			content: 'select-project',
		})

	},

	// Edit / Update Project

	showEditProject(store, project) {

		store.commit('SHOW_MODAL', {
			content: 'edit-project',
			project: project,
		})

	},

	// Remove Project

	showRemoveProject(store, project) {

		// store.state.temp.project = project

		store.commit('SHOW_MODAL', {
			content: 'remove-project',
			project: project,
		})

	},

	// Add New Project

	showAddNewProject(store) {

		store.commit('SHOW_MODAL', {
			content: 'add-new-project'
		})

	},

	// Remove Todo

	showRemoveTodo(store, todo) {

		store.commit('SHOW_MODAL', {
			content: 'remove-todo',
			todo: todo,
		})

	},

	// Add New Todo

	showAddNewTodo(store) {

		store.commit('SHOW_MODAL', {
			content: 'add-new-todo'
		})

	},

	// Edit / Update Todo

	showEditTodo(store, todo) {

		store.commit('SHOW_MODAL', {
			content: 'edit-todo',
			todo: todo,
		})

	},

	// Messages

	showMessage(store, message) {

		store.commit('SHOW_MESSAGE', message)

		clearTimeout(store.state.ui.message.timer)
		store.state.ui.message.timer = setTimeout(function() {
			store.commit('HIDE_MESSAGE')
			clearTimeout(store.state.ui.message.timer)
		}, 5000)


	},

}