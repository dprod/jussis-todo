
export default {

	getSelectedProject: state => {
		return state.projects.find((project) => project.slug === state.selectedProjectSlug)
	},

}
