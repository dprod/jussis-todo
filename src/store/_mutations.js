
import Vue from 'vue'

export default {

	INIT(state, payload) {

		state.init = payload

	},

	SET_PROJECTS(state, projects) {

		state.projects = projects

	},

	////////////////////////////////////////////////////////////////////////////

	SET_SELECTED_PROJECT_SLUG(state, slug) {

		// #TODO: Make sure slugs are unique (front+back)

		state.selectedProjectSlug = slug

	},

	// DATA EDITING

	ADD_NEW_PROJECT(state, project) {

		state.projects.push(project)

	},

	UPDATE_PROJECT(state, { oldProject, newProject }) {

		oldProject = newProject

	},

	REMOVE_PROJECT(state, project) {

		state.projects.splice(state.projects.indexOf(project), 1)

		state.projects.forEach((project) => Vue.set(project, 'selected', false))

	},

	ADD_NEW_TODO(state, { project, todo }) {

		project.todos.push(todo)

	},

	UPDATE_TODO(state, { oldTodo, newTodo }) {

		oldTodo = newTodo

	},

	REMOVE_TODO(state, { project, todo }) {

		project.todos.splice(project.todos.indexOf(todo), 1)

	},

	COMPLETE_TODO(state, todo) {

		Vue.set(todo, 'completed', true)

	},

	UNCOMPLETE_TODO(state, todo) {

		Vue.set(todo, 'completed', false)

	},

	// MODAL

	SHOW_MODAL(state, { content, project, todo }) {

		state.ui.showModal = true

		// Set temp project and todo values if sent.
		state.temp.project = project ? project : {}
		state.temp.todo = todo ? todo : {}

		// Ensure only 1 is showed at once
		Object.keys(state.ui.modals).forEach((key) => state.ui.modals[key] = false)

		if(content === 'select-project')
			state.ui.modals.showSelectProject = true

		if(content === 'add-new-project')
			state.ui.modals.showAddNewProject = true
		if(content === 'edit-project')
			state.ui.modals.showEditProject = true
		if(content === 'remove-project')
			state.ui.modals.showRemoveProject = true

		if(content === 'add-new-todo')
			state.ui.modals.showAddNewTodo = true
		if(content === 'edit-todo')
			state.ui.modals.showEditTodo = true
		if(content === 'remove-todo')
			state.ui.modals.showRemoveTodo = true

	},

	HIDE_MODAL(state) {

		state.ui.showModal = false

		Object.keys(state.ui.modals).forEach((key) => state.ui.modals[key] = false)

		// Clear temp project and todo objects on modal close.
		// #TODO: Better implemenetation?
		Object.keys(state.temp).forEach((key) => state.temp[key] = {})

	},

	SHOW_MESSAGE(state, message) {

		state.ui.message.show = true
		state.ui.message.text = message

	},

	HIDE_MESSAGE(state) {

		state.ui.message.show = false
		state.ui.message.message = ''

	},




}