export default {

	init: false,

	selectedProjectSlug: '',		// Get from route param, use to get selected project

	ui: {

		showModal: false,

		modals: {

			showSelectProject: false,

			showAddNewProject: false,
			showEditProject: false,
			showRemoveProject: false,

			showAddNewTodo: false,
			showEditTodo: false,
			showRemoveTodo: false,

		},

		message: {
			show: false,
			text: '',
			timer: 0,
		},

	},

	temp: {
		project: {},
		todo: {},
	},

	projects: [],
	
}